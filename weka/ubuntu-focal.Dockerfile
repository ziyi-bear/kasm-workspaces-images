FROM kasmweb/ubuntu-focal-desktop:1.12.0
USER root

ENV HOME /home/kasm-default-profile
ENV STARTUPDIR /dockerstartup
ENV INST_SCRIPTS $STARTUPDIR/install
WORKDIR $HOME

######### Customize Container Here ###########


######### End Customizations ###########

RUN chown 1000:0 $HOME
RUN $STARTUPDIR/set_user_permission.sh $HOME

ENV HOME /home/kasm-user
WORKDIR $HOME
RUN mkdir -p $HOME && chown -R 1000:0 $HOME

USER 1000

######### Customize Container Here Normal Content ###########

## https://waikato.github.io/weka-wiki/downloading_weka/
## 安裝Weka(主要透過下載執行包方式)
RUN wget https://prdownloads.sourceforge.net/weka/weka-3-8-6-azul-zulu-linux.zip
RUN unzip weka-3-8-6-azul-zulu-linux.zip
RUN mv weka-3-8-6 Desktop/