FROM kasmweb/ubuntu-focal-desktop:1.12.0
USER root

ENV HOME /home/kasm-default-profile
ENV STARTUPDIR /dockerstartup
ENV INST_SCRIPTS $STARTUPDIR/install
WORKDIR $HOME

######### Customize Container Here ###########

## 安裝Python所需執行的相依套件
RUN apt update && \
    apt install -y nano python3-pip 
## 安裝QT5
RUN apt update && \
    apt install -y qt5dxcb-plugin \
    python3-pyqt5*

## 安裝瀏覽器方便線上看Orange3教學影片
## https://itsfoss.com/install-chrome-ubuntu/
## dpkg -i google-chrome-stable_current_amd64.deb
RUN apt update && \
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
    apt install -y ./google-chrome-stable_current_amd64.deb

## 安裝Orange3
RUN pip install orange3
#RUN pip install PyQt5 PyQtWebEngine && \
#    pip install orange3

######### End Customizations ###########

RUN chown 1000:0 $HOME
RUN $STARTUPDIR/set_user_permission.sh $HOME

ENV HOME /home/kasm-user
WORKDIR $HOME
RUN mkdir -p $HOME && chown -R 1000:0 $HOME

USER 1000
